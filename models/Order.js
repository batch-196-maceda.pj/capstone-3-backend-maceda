/*
	Model for orders collection
*/
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount: {
		type: Number,
		required: [true,"Amount required"]
	},
    purchasedOn: {
		type: Date,
		default: new Date()
	},
    userId: {
        type: String,
        required: [true, "User Id required"]
    },
	products: [

        {
            productId: {
                type: String,
                required: [true,"Product Id required"]
            },
            quantity: {
                type: Number,
                default: 1
            }
        }

    ]
	})

	module.exports = mongoose.model("Order",orderSchema);