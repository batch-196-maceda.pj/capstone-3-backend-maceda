const express = require("express");
const router = express.Router();

const orderControllers = require("../controllers/orderControllers");

const auth = require("../auth");

const { verify } = auth;

const { verifyAdmin } = auth;

//view orders by the user
router.get("/", verify, orderControllers.viewOrders);

//view all orders as admin
router.get("/viewAll",verify, verifyAdmin, orderControllers.viewAll)

//create order
router.post("/createOrder/:productId", verify, orderControllers.createOrder);

//view products per order
router.get("/view/:orderId", verify, orderControllers.viewProductsPerOrder)


module.exports = router;