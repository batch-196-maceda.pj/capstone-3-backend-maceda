const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

const auth = require("../auth");

const { verify } = auth;

const { verifyAdmin } = auth;

const { verifyFullControl } = auth;

//view all users, admin only-admin perks
router.get("/", verify, verifyAdmin, userControllers.viewAll);

//register
router.post("/register",userControllers.register);

// login
router.post("/login", userControllers.login);

// get user details-tbu later for profile fetching
router.get("/getUserDetails", verify, userControllers.viewUser);

// set as admin-addmin perks
router.put("/setAdmin/:userId", verify, verifyAdmin, userControllers.setAdmin);

// remove as admin
router.put("/removeAdmin/:userId", verify, verifyAdmin, userControllers.removeAdmin);

// check if email exists
router.post('/checkEmailExists',userControllers.checkEmailExists);

module.exports = router;