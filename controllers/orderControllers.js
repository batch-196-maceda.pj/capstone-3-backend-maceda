const Order = require("../models/Order");

const Product = require("../models/Product");

// view orders for admin
module.exports.viewAll = (req,res)=>{
    // finds all orders
	Order.find({})
	.then(result => {
		if(result === null){
			res.send("No existing orders yet.")
		} else {
			res.send(result)
		}
	})
	.catch(error => res.send(error))
};

// view orders for user
module.exports.viewOrders = (req,res)=>{
    // finds all orders but only return amount, date of purchase, and products
	Order.find({userId:req.user.id}, "totalAmount purchasedOn products _id")
	.then(result => {
		if(result === null){
			res.send("No existing orders yet.")
		} else {
			res.send(result)
		}
	})
	.catch(error => res.send(error))
};

// display products per order
module.exports.viewProductsPerOrder = (req,res)=>{

	Order.findOne({_id:req.params.orderId, userId:req.user.id},"products")
	.then(result => {
		if(result === null){
			res.send("No such order exists.")
		} else {
			res.send(result)
		}

	}).catch(error => res.send(error))
};

// creating orders
module.exports.createOrder = async (req,res)=>{
	if(req.user.isAdmin ===true ){
		return res.send({message: "Action Forbidden."});
	} else {
		// for calculating total amount
		price = []
		for(key in req.body){
			price.push(await Product.findOne({_id:req.body[key].productId,isActive:true}).then(result => {
						let amount = result.price*req.body[key].quantity
						result.numberOfOrders += req.body[key].quantity
						result.save()
						return amount
					}).catch(error => console.log(error)))
			}

		updateTotalAmount = price.reduce((a,b) => a+b,0)

		console.log(req.body)
		let newOrder = new Order({
			totalAmount: updateTotalAmount,
			userId: req.user.id,
			products: req.body

			
		})
	
		newOrder.save().then(result => res.send(result)).catch(error =>

		{console.log(error)
			res.send(true)})

	}
}
